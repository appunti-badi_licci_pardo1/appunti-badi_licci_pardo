﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appunti_Badi_Licci_Pardo
{
    static class Program
    {
        static public int _matricola = 0;
        static public List<ClsUtente> _listaUtenti = new List<ClsUtente>();
        static public List<string> _listaMaterie = new List<string>();
        /// <summary>
        /// Punto di ingresso principale dell'applicazione.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmMenu());
        }
    }
}
