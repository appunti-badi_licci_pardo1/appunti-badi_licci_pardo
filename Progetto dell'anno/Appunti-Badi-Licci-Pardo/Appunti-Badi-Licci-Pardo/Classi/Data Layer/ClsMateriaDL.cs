﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appunti_Badi_Licci_Pardo
{
    class ClsMateriaDL
    {
        private string _codiceMateria;
        private string _nome;

        public string CodiceMateria { get => _codiceMateria; set => _codiceMateria = value; }
        public string Nome { get => _nome; set => _nome = value; }

        public ClsMateriaDL(string codiceMateria, string nome)
        {
            CodiceMateria = _codiceMateria;
            Nome = _nome;
        }
    }
}
