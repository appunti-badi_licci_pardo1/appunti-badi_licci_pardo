﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appunti_Badi_Licci_Pardo
{
    class ClsAppuntoDL
    {
        public enum eMATERIA
        {
            Italiano,
            Storia,
            Informatica,
            Matematica
        }


        public enum eLINGUA
        {
            Italiano,
            English,
            Francois,
            Espanol
        }

        private string _titolo;
        private string _autore;
        private DateTime _ultimaModifica;
        private string _argomento;
        private eMATERIA _materia;
        private string _contenuto;
        private eLINGUA _lingua;

    }
}
