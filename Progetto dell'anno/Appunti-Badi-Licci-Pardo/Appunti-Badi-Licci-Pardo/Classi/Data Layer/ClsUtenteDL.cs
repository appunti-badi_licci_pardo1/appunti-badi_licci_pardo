﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appunti_Badi_Licci_Pardo
{
    //CREATORE: BADI FEDERICO

    class ClsUtenteDL
    {
        private string _cognome;
        private string _nome;
        private int _matricola;
        private string _password;
        private string _email;

        public string Cognome { get => _cognome; set => _cognome = value; }
        public string Nome { get => _nome; set => _nome = value; }
        public int Matricola { get => _matricola; set => _matricola = value; }
        public string Password { get => _password; set => _password = value; }
        public string Email { get => _email; set => _email = value; }

        public ClsUtenteDL(string cognome, string nome, string password, string email)
        {
            Nome = nome;
            Cognome = cognome;
            Password = password;
            Email = email;
            Matricola = Program._matricola;
        }
    }
}
