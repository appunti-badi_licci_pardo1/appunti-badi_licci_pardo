﻿namespace Appunti_Badi_Licci_Pardo
{
    partial class FrmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMenu));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.panelAISubMenu = new System.Windows.Forms.Panel();
            this.btFlashBud = new System.Windows.Forms.Button();
            this.FlashBud = new System.Windows.Forms.Button();
            this.panelAppunti = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btCreaAppunti = new System.Windows.Forms.Button();
            this.btVisualizzaAppunti = new System.Windows.Forms.Button();
            this.btAppunti = new System.Windows.Forms.Button();
            this.btAiuto = new System.Windows.Forms.Button();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.labelTitolo = new System.Windows.Forms.Label();
            this.panelPerLogo = new System.Windows.Forms.Panel();
            this.panelFormFiglia = new System.Windows.Forms.Panel();
            this.panelSideMenu.SuspendLayout();
            this.panelAISubMenu.SuspendLayout();
            this.panelAppunti.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelLogo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.SystemColors.Highlight;
            this.panelSideMenu.Controls.Add(this.panelAISubMenu);
            this.panelSideMenu.Controls.Add(this.FlashBud);
            this.panelSideMenu.Controls.Add(this.panelAppunti);
            this.panelSideMenu.Controls.Add(this.btAppunti);
            this.panelSideMenu.Controls.Add(this.btAiuto);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(194, 653);
            this.panelSideMenu.TabIndex = 0;
            this.panelSideMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSideMenu_Paint);
            // 
            // panelAISubMenu
            // 
            this.panelAISubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelAISubMenu.Controls.Add(this.btFlashBud);
            this.panelAISubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAISubMenu.Location = new System.Drawing.Point(0, 230);
            this.panelAISubMenu.Margin = new System.Windows.Forms.Padding(2);
            this.panelAISubMenu.Name = "panelAISubMenu";
            this.panelAISubMenu.Size = new System.Drawing.Size(194, 37);
            this.panelAISubMenu.TabIndex = 12;
            // 
            // btFlashBud
            // 
            this.btFlashBud.BackColor = System.Drawing.Color.White;
            this.btFlashBud.Dock = System.Windows.Forms.DockStyle.Top;
            this.btFlashBud.FlatAppearance.BorderSize = 0;
            this.btFlashBud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFlashBud.Location = new System.Drawing.Point(0, 0);
            this.btFlashBud.Margin = new System.Windows.Forms.Padding(2);
            this.btFlashBud.Name = "btFlashBud";
            this.btFlashBud.Padding = new System.Windows.Forms.Padding(26, 0, 0, 0);
            this.btFlashBud.Size = new System.Drawing.Size(194, 37);
            this.btFlashBud.TabIndex = 0;
            this.btFlashBud.Text = "Prova Chat GPT integrata";
            this.btFlashBud.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFlashBud.UseVisualStyleBackColor = false;
            this.btFlashBud.Click += new System.EventHandler(this.btFlashBud_Click);
            // 
            // FlashBud
            // 
            this.FlashBud.BackColor = System.Drawing.SystemColors.Highlight;
            this.FlashBud.Dock = System.Windows.Forms.DockStyle.Top;
            this.FlashBud.FlatAppearance.BorderSize = 0;
            this.FlashBud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FlashBud.ForeColor = System.Drawing.Color.White;
            this.FlashBud.Location = new System.Drawing.Point(0, 193);
            this.FlashBud.Margin = new System.Windows.Forms.Padding(2);
            this.FlashBud.Name = "FlashBud";
            this.FlashBud.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.FlashBud.Size = new System.Drawing.Size(194, 37);
            this.FlashBud.TabIndex = 13;
            this.FlashBud.Text = "Chat GPT";
            this.FlashBud.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FlashBud.UseVisualStyleBackColor = false;
            this.FlashBud.Click += new System.EventHandler(this.FlashBud_Click);
            // 
            // panelAppunti
            // 
            this.panelAppunti.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelAppunti.Controls.Add(this.pictureBox2);
            this.panelAppunti.Controls.Add(this.pictureBox1);
            this.panelAppunti.Controls.Add(this.btCreaAppunti);
            this.panelAppunti.Controls.Add(this.btVisualizzaAppunti);
            this.panelAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAppunti.Location = new System.Drawing.Point(0, 118);
            this.panelAppunti.Margin = new System.Windows.Forms.Padding(2);
            this.panelAppunti.Name = "panelAppunti";
            this.panelAppunti.Size = new System.Drawing.Size(194, 75);
            this.panelAppunti.TabIndex = 1;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 35);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(49, 40);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(49, 37);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // btCreaAppunti
            // 
            this.btCreaAppunti.BackColor = System.Drawing.Color.White;
            this.btCreaAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.btCreaAppunti.FlatAppearance.BorderSize = 0;
            this.btCreaAppunti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCreaAppunti.Location = new System.Drawing.Point(0, 37);
            this.btCreaAppunti.Margin = new System.Windows.Forms.Padding(2);
            this.btCreaAppunti.Name = "btCreaAppunti";
            this.btCreaAppunti.Padding = new System.Windows.Forms.Padding(26, 0, 0, 0);
            this.btCreaAppunti.Size = new System.Drawing.Size(194, 37);
            this.btCreaAppunti.TabIndex = 1;
            this.btCreaAppunti.Text = "         Crea Appunti";
            this.btCreaAppunti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCreaAppunti.UseVisualStyleBackColor = false;
            this.btCreaAppunti.Click += new System.EventHandler(this.btCreaAppunti_Click);
            // 
            // btVisualizzaAppunti
            // 
            this.btVisualizzaAppunti.BackColor = System.Drawing.Color.White;
            this.btVisualizzaAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.btVisualizzaAppunti.FlatAppearance.BorderSize = 0;
            this.btVisualizzaAppunti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btVisualizzaAppunti.Location = new System.Drawing.Point(0, 0);
            this.btVisualizzaAppunti.Margin = new System.Windows.Forms.Padding(2);
            this.btVisualizzaAppunti.Name = "btVisualizzaAppunti";
            this.btVisualizzaAppunti.Padding = new System.Windows.Forms.Padding(26, 0, 0, 0);
            this.btVisualizzaAppunti.Size = new System.Drawing.Size(194, 37);
            this.btVisualizzaAppunti.TabIndex = 0;
            this.btVisualizzaAppunti.Text = "       Appunti globali";
            this.btVisualizzaAppunti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btVisualizzaAppunti.UseVisualStyleBackColor = false;
            this.btVisualizzaAppunti.Click += new System.EventHandler(this.btVisualizzaAppunti_Click_1);
            // 
            // btAppunti
            // 
            this.btAppunti.BackColor = System.Drawing.SystemColors.Highlight;
            this.btAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.btAppunti.FlatAppearance.BorderSize = 0;
            this.btAppunti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAppunti.ForeColor = System.Drawing.Color.White;
            this.btAppunti.Location = new System.Drawing.Point(0, 81);
            this.btAppunti.Margin = new System.Windows.Forms.Padding(2);
            this.btAppunti.Name = "btAppunti";
            this.btAppunti.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btAppunti.Size = new System.Drawing.Size(194, 37);
            this.btAppunti.TabIndex = 9;
            this.btAppunti.Text = "Appunti";
            this.btAppunti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAppunti.UseVisualStyleBackColor = false;
            this.btAppunti.Click += new System.EventHandler(this.btAppunti_Click_1);
            // 
            // btAiuto
            // 
            this.btAiuto.FlatAppearance.BorderSize = 0;
            this.btAiuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAiuto.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btAiuto.Location = new System.Drawing.Point(0, 563);
            this.btAiuto.Margin = new System.Windows.Forms.Padding(2);
            this.btAiuto.Name = "btAiuto";
            this.btAiuto.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btAiuto.Size = new System.Drawing.Size(188, 37);
            this.btAiuto.TabIndex = 8;
            this.btAiuto.Text = "Aiuto e report bug";
            this.btAiuto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAiuto.UseVisualStyleBackColor = true;
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.SystemColors.Highlight;
            this.panelLogo.Controls.Add(this.labelTitolo);
            this.panelLogo.Controls.Add(this.panelPerLogo);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Margin = new System.Windows.Forms.Padding(2);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(194, 81);
            this.panelLogo.TabIndex = 0;
            // 
            // labelTitolo
            // 
            this.labelTitolo.AutoSize = true;
            this.labelTitolo.Font = new System.Drawing.Font("Broadway", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitolo.ForeColor = System.Drawing.Color.White;
            this.labelTitolo.Location = new System.Drawing.Point(46, 34);
            this.labelTitolo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelTitolo.Name = "labelTitolo";
            this.labelTitolo.Size = new System.Drawing.Size(98, 25);
            this.labelTitolo.TabIndex = 2;
            this.labelTitolo.Text = "MATCH";
            // 
            // panelPerLogo
            // 
            this.panelPerLogo.BackgroundImage = global::Appunti_Badi_Licci_Pardo.Properties.Resources.LOGO3;
            this.panelPerLogo.Location = new System.Drawing.Point(2, 21);
            this.panelPerLogo.Margin = new System.Windows.Forms.Padding(2);
            this.panelPerLogo.Name = "panelPerLogo";
            this.panelPerLogo.Size = new System.Drawing.Size(39, 39);
            this.panelPerLogo.TabIndex = 1;
            // 
            // panelFormFiglia
            // 
            this.panelFormFiglia.Location = new System.Drawing.Point(194, 0);
            this.panelFormFiglia.Name = "panelFormFiglia";
            this.panelFormFiglia.Size = new System.Drawing.Size(1920, 1080);
            this.panelFormFiglia.TabIndex = 1;
            // 
            // FrmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1396, 653);
            this.Controls.Add(this.panelFormFiglia);
            this.Controls.Add(this.panelSideMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(716, 493);
            this.Name = "FrmMenu";
            this.Resize += new System.EventHandler(this.FrmMenu_Resize);
            this.panelSideMenu.ResumeLayout(false);
            this.panelAISubMenu.ResumeLayout(false);
            this.panelAppunti.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelLogo.ResumeLayout(false);
            this.panelLogo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button btAiuto;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Button btAppunti;
        private System.Windows.Forms.Panel panelAppunti;
        private System.Windows.Forms.Button btCreaAppunti;
        private System.Windows.Forms.Button btVisualizzaAppunti;
        private System.Windows.Forms.Panel panelAISubMenu;
        private System.Windows.Forms.Button btFlashBud;
        private System.Windows.Forms.Button FlashBud;
        private System.Windows.Forms.Panel panelPerLogo;
        private System.Windows.Forms.Label labelTitolo;
        private System.Windows.Forms.Panel panelFormFiglia;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}