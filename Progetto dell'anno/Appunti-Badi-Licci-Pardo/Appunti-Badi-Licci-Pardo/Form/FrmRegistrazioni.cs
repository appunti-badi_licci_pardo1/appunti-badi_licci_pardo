﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;   // Include dll opportuna per lavorare con MySql

using System.Configuration;     // Questa libreria ci permette di lavorare con il file di configurazione (in alternativa posso usare Properties > Settings)

using System.Net.Mail;


namespace Appunti_Badi_Licci_Pardo
{
    //CREATORE: BADI FEDERICO
    public partial class FrmRegistrazioni : Form
    {
        public FrmRegistrazioni()
        {
            InitializeComponent();
            
        }

        private void btnRegistrati_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(tbNome.Text) || string.IsNullOrWhiteSpace(tbCognome.Text) || string.IsNullOrWhiteSpace(tbPassword.Text) || string.IsNullOrWhiteSpace(tbConPassword.Text) || string.IsNullOrWhiteSpace(tbEmail.Text))
            {
                MessageBox.Show("I dati inseriti non sono validi");
                TextBox[] textBoxes = { tbNome, tbCognome, tbPassword, tbConPassword, tbEmail };

                foreach (TextBox textBox in textBoxes)
                {
                    textBox.Text = "";
                }

            }
            else
            {
                string _nome = tbNome.Text;
                string _cogome = tbCognome.Text;
                string _password = tbPassword.Text.Trim();
                string _confPassword = tbConPassword.Text.Trim();
                string _email = tbEmail.Text.Trim();

                if (!_confPassword.Equals(_password))
                {
                    _password = string.Empty;
                    _confPassword = string.Empty;
                    MessageBox.Show("Le Password non corrispondono");
                    tbPassword.Text = string.Empty;
                    tbConPassword.Text = string.Empty;
                }
                else
                {
                    if (ControlloEmail(_email))
                    {
                        ClsUtente clsUtente = new ClsUtente(_cogome, _nome, _password, _email);
                        Program._listaUtenti.Add(clsUtente);
                        MessageBox.Show("Registrazione completata");
                        Program._matricola++;
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("Indirizzo email non valido");
                        tbEmail.Text = string.Empty;
                    }

                }
            }
            //string miaConnectionString = ConfigurationManager.ConnectionStrings["appunti"].ConnectionString;
            //MySqlConnection conn = new MySqlConnection(miaConnectionString);

            try
            {
                //2) Apertura connessione
                //conn.Open();

                //3) Lavoro con il DB




                //4) Chiusura connessione
                //conn.Close(); 
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Test di connessione fallito! {ex.Message} ");
            }


        }
        private bool ControlloEmail(string email)
        {
            try
            {
                MailAddress _indirizzoEmail = new MailAddress(email);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void lblChiudi_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
