﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appunti_Badi_Licci_Pardo
{
    //CREATORE: BADI FEDERICO
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void lblRegistrazione_Click(object sender, EventArgs e)
        {
            TextBox[] textBoxes = { tbEmail, tbPassword };
            foreach (TextBox textBox in textBoxes)
            {
                textBox.Text = "";
            }
            FrmRegistrazioni frmRegistrati = new FrmRegistrazioni();
            DialogResult dr = frmRegistrati.ShowDialog(this);

        }

        private void lblEsci_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbEmail.Text) || string.IsNullOrWhiteSpace(tbPassword.Text))
            {
                MessageBox.Show("I dati inseriti non sono validi");
                TextBox[] textBoxes = { tbEmail, tbPassword };
                btMostraPassword.BringToFront();

                foreach (TextBox textBox in textBoxes)
                {
                    textBox.Text = "";
                }
            }
            else
            {
                string _email = tbEmail.Text;
                try
                {
                    string _appoggio = Program._listaUtenti.FirstOrDefault(_app => _app.Email == _email)?.ToString();
                    if (_appoggio != null)
                    {
                        // Trova l'utente con l'email specificata
                        ClsUtente utenteTrovato = Program._listaUtenti.FirstOrDefault(_app => _app.Email == _email);
                        if (utenteTrovato != null)
                        {
                            // Ottieni l'indice dell'utente trovato nella lista
                            int indice = Program._listaUtenti.IndexOf(utenteTrovato);

                            // Controlla se l'indice è valido
                            if (indice != -1 && indice < Program._listaUtenti.Count)
                            {
                                string password = Program._listaUtenti[indice].Password;
                                if(password.Equals(tbPassword.Text))
                                {
                                    FrmMenu frmMenù = new FrmMenu();
                                    DialogResult dr = frmMenù.ShowDialog(this);
                                }
                                else
                                {
                                    MessageBox.Show("Password sbagliata");
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Utente non trovato");
                        }

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Registrati prima di accedere");
                }
            }
        }

        private void btMostraPassword_Click(object sender, EventArgs e)
        {
            if (tbPassword.PasswordChar == '*')
            {
                btNascondiPassword.BringToFront();
                tbPassword.PasswordChar = '\0';
            }
           
            
        }

        private void btNascondiPassword_Click(object sender, EventArgs e)
        {
            if (tbPassword.PasswordChar == '\0')
            {
                btMostraPassword.BringToFront();
                tbPassword.PasswordChar = '*';
            }
        }
    }
    
}
