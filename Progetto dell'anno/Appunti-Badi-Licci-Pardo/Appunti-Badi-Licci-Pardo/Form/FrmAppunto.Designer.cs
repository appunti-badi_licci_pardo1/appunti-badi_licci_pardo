﻿namespace Appunti_Badi_Licci_Pardo
{
    partial class FrmAppunto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAppunto));
            this.tbTitolo = new System.Windows.Forms.TextBox();
            this.tbAppunto = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ElencoNote = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.tbArgomento = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbMaterie = new System.Windows.Forms.ComboBox();
            this.PbCaricaAppunto = new Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox();
            this.PbNuovoAppunto = new Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox();
            this.PbSalvaAppunto = new Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox();
            this.PbEliminaAppunto = new Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox();
            this.PbCancellaTesto = new Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox();
            this.siticoneCirclePictureBox1 = new Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ElencoNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbCaricaAppunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbNuovoAppunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbSalvaAppunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbEliminaAppunto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbCancellaTesto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.siticoneCirclePictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tbTitolo
            // 
            this.tbTitolo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbTitolo.Location = new System.Drawing.Point(693, 11);
            this.tbTitolo.Margin = new System.Windows.Forms.Padding(2);
            this.tbTitolo.Name = "tbTitolo";
            this.tbTitolo.Size = new System.Drawing.Size(225, 20);
            this.tbTitolo.TabIndex = 0;
            // 
            // tbAppunto
            // 
            this.tbAppunto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbAppunto.Location = new System.Drawing.Point(495, 184);
            this.tbAppunto.Margin = new System.Windows.Forms.Padding(2);
            this.tbAppunto.Multiline = true;
            this.tbAppunto.Name = "tbAppunto";
            this.tbAppunto.Size = new System.Drawing.Size(674, 428);
            this.tbAppunto.TabIndex = 1;
            this.tbAppunto.TextChanged += new System.EventHandler(this.tbAppunto_TextChanged);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(601, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Titolo:";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(491, 150);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "Appunto";
            // 
            // ElencoNote
            // 
            this.ElencoNote.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.ElencoNote.BackgroundColor = System.Drawing.Color.White;
            this.ElencoNote.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ElencoNote.Location = new System.Drawing.Point(9, 10);
            this.ElencoNote.Margin = new System.Windows.Forms.Padding(2);
            this.ElencoNote.Name = "ElencoNote";
            this.ElencoNote.RowTemplate.Height = 24;
            this.ElencoNote.Size = new System.Drawing.Size(478, 602);
            this.ElencoNote.TabIndex = 4;
            this.ElencoNote.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ElencoNote_CellContentClick);
            this.ElencoNote.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.ElencoNote_CellDoubleClick);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(568, 49);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 31);
            this.label3.TabIndex = 11;
            this.label3.Text = "Argomento:";
            // 
            // tbArgomento
            // 
            this.tbArgomento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbArgomento.Location = new System.Drawing.Point(693, 55);
            this.tbArgomento.Margin = new System.Windows.Forms.Padding(2);
            this.tbArgomento.Name = "tbArgomento";
            this.tbArgomento.Size = new System.Drawing.Size(225, 20);
            this.tbArgomento.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(589, 89);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 31);
            this.label4.TabIndex = 12;
            this.label4.Text = "Materia:";
            // 
            // cbMaterie
            // 
            this.cbMaterie.FormattingEnabled = true;
            this.cbMaterie.Location = new System.Drawing.Point(693, 95);
            this.cbMaterie.Name = "cbMaterie";
            this.cbMaterie.Size = new System.Drawing.Size(225, 21);
            this.cbMaterie.TabIndex = 14;
            this.cbMaterie.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // PbCaricaAppunto
            // 
            this.PbCaricaAppunto.Image = ((System.Drawing.Image)(resources.GetObject("PbCaricaAppunto.Image")));
            this.PbCaricaAppunto.ImageRotate = 0F;
            this.PbCaricaAppunto.Location = new System.Drawing.Point(487, 80);
            this.PbCaricaAppunto.Name = "PbCaricaAppunto";
            this.PbCaricaAppunto.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.PbCaricaAppunto.Size = new System.Drawing.Size(45, 36);
            this.PbCaricaAppunto.TabIndex = 15;
            this.PbCaricaAppunto.TabStop = false;
            this.PbCaricaAppunto.Click += new System.EventHandler(this.siticoneCirclePictureBox1_Click);
            // 
            // PbNuovoAppunto
            // 
            this.PbNuovoAppunto.Image = ((System.Drawing.Image)(resources.GetObject("PbNuovoAppunto.Image")));
            this.PbNuovoAppunto.ImageRotate = 0F;
            this.PbNuovoAppunto.Location = new System.Drawing.Point(492, 11);
            this.PbNuovoAppunto.Name = "PbNuovoAppunto";
            this.PbNuovoAppunto.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.PbNuovoAppunto.Size = new System.Drawing.Size(36, 35);
            this.PbNuovoAppunto.TabIndex = 16;
            this.PbNuovoAppunto.TabStop = false;
            this.PbNuovoAppunto.Click += new System.EventHandler(this.siticoneCirclePictureBox2_Click);
            // 
            // PbSalvaAppunto
            // 
            this.PbSalvaAppunto.Image = ((System.Drawing.Image)(resources.GetObject("PbSalvaAppunto.Image")));
            this.PbSalvaAppunto.ImageRotate = 0F;
            this.PbSalvaAppunto.Location = new System.Drawing.Point(1076, 143);
            this.PbSalvaAppunto.Name = "PbSalvaAppunto";
            this.PbSalvaAppunto.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.PbSalvaAppunto.Size = new System.Drawing.Size(51, 35);
            this.PbSalvaAppunto.TabIndex = 17;
            this.PbSalvaAppunto.TabStop = false;
            this.PbSalvaAppunto.Click += new System.EventHandler(this.siticoneCirclePictureBox3_Click);
            // 
            // PbEliminaAppunto
            // 
            this.PbEliminaAppunto.Image = ((System.Drawing.Image)(resources.GetObject("PbEliminaAppunto.Image")));
            this.PbEliminaAppunto.ImageRotate = 0F;
            this.PbEliminaAppunto.Location = new System.Drawing.Point(492, 45);
            this.PbEliminaAppunto.Name = "PbEliminaAppunto";
            this.PbEliminaAppunto.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.PbEliminaAppunto.Size = new System.Drawing.Size(36, 35);
            this.PbEliminaAppunto.TabIndex = 18;
            this.PbEliminaAppunto.TabStop = false;
            this.PbEliminaAppunto.Click += new System.EventHandler(this.siticoneCirclePictureBox4_Click);
            // 
            // PbCancellaTesto
            // 
            this.PbCancellaTesto.Image = ((System.Drawing.Image)(resources.GetObject("PbCancellaTesto.Image")));
            this.PbCancellaTesto.ImageRotate = 0F;
            this.PbCancellaTesto.Location = new System.Drawing.Point(1133, 147);
            this.PbCancellaTesto.Name = "PbCancellaTesto";
            this.PbCancellaTesto.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.PbCancellaTesto.Size = new System.Drawing.Size(36, 35);
            this.PbCancellaTesto.TabIndex = 19;
            this.PbCancellaTesto.TabStop = false;
            this.PbCancellaTesto.Click += new System.EventHandler(this.PbCancellaTesto_Click);
            // 
            // siticoneCirclePictureBox1
            // 
            this.siticoneCirclePictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("siticoneCirclePictureBox1.Image")));
            this.siticoneCirclePictureBox1.ImageRotate = 0F;
            this.siticoneCirclePictureBox1.Location = new System.Drawing.Point(922, 87);
            this.siticoneCirclePictureBox1.Name = "siticoneCirclePictureBox1";
            this.siticoneCirclePictureBox1.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.siticoneCirclePictureBox1.Size = new System.Drawing.Size(36, 35);
            this.siticoneCirclePictureBox1.TabIndex = 20;
            this.siticoneCirclePictureBox1.TabStop = false;
            this.siticoneCirclePictureBox1.Click += new System.EventHandler(this.siticoneCirclePictureBox1_Click_1);
            // 
            // FrmAppunto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1186, 627);
            this.Controls.Add(this.siticoneCirclePictureBox1);
            this.Controls.Add(this.PbCancellaTesto);
            this.Controls.Add(this.PbEliminaAppunto);
            this.Controls.Add(this.PbSalvaAppunto);
            this.Controls.Add(this.PbNuovoAppunto);
            this.Controls.Add(this.PbCaricaAppunto);
            this.Controls.Add(this.cbMaterie);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbArgomento);
            this.Controls.Add(this.ElencoNote);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbAppunto);
            this.Controls.Add(this.tbTitolo);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmAppunto";
            this.Text = "FrmAppunto";
            this.Load += new System.EventHandler(this.FrmAppunto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ElencoNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbCaricaAppunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbNuovoAppunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbSalvaAppunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbEliminaAppunto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PbCancellaTesto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.siticoneCirclePictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTitolo;
        private System.Windows.Forms.TextBox tbAppunto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView ElencoNote;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbArgomento;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbMaterie;
        private Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox PbCaricaAppunto;
        private Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox PbNuovoAppunto;
        private Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox PbSalvaAppunto;
        private Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox PbEliminaAppunto;
        private Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox PbCancellaTesto;
        private Siticone.Desktop.UI.WinForms.SiticoneCirclePictureBox siticoneCirclePictureBox1;
    }
}