﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;   // Include dll opportuna per lavorare con MySql
using System.Configuration;     // Questa libreria ci permette di lavorare con il file di configurazione (in alternativa posso usare Properties > Settings)



namespace Appunti_Badi_Licci_Pardo
{
    public partial class FrmAppunto : Form
    {
        DataTable Note = new DataTable();
        bool Modifica = false;
        public FrmAppunto()
        {
            InitializeComponent();
            //_materie = new List<string>();
            //popolaCB();
        }
        

        private void popolaCB()
        {
            cbMaterie.Items.Clear();
            foreach (var item in Program._listaMaterie)
            {
                cbMaterie.Items.Add(item);
            }

        }

        private void FrmAppunto_Load(object sender, EventArgs e)
        {
            Note.Columns.Add("Titolo");
            Note.Columns.Add("Appunto");

            ElencoNote.DataSource = Note;   
        }

        private void btCrea_Click(object sender, EventArgs e)
        {
            tbTitolo.Text = "";
            tbAppunto.Text = "";
        }

        private void btElimina_Click(object sender, EventArgs e)
        {
            try
            {
                Note.Rows[ElencoNote.CurrentCell.RowIndex].Delete();
            }
            catch (Exception ex) { Console.WriteLine("Nota non valida"); }
            
        }

        private void btSalva_Click(object sender, EventArgs e)
        {
            if (Modifica)
            {
                Note.Rows[ElencoNote.CurrentCell.RowIndex]["Titolo"] = tbTitolo.Text;
                Note.Rows[ElencoNote.CurrentCell.RowIndex]["Appunto"] = tbAppunto.Text;

            }
            else
            {
                Note.Rows.Add(tbTitolo.Text, tbAppunto.Text);
            }
            tbTitolo.Text = "";
            tbAppunto.Text = "";
            Modifica = false;





        }

        private void btCarica_Click(object sender, EventArgs e)
        {
            tbTitolo.Text = Note.Rows[ElencoNote.CurrentCell.RowIndex].ItemArray[0].ToString();
            tbAppunto.Text = Note.Rows[ElencoNote.CurrentCell.RowIndex].ItemArray[1].ToString();
            Modifica = true;

        }

        private void ElencoNote_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            tbTitolo.Text = Note.Rows[ElencoNote.CurrentCell.RowIndex].ItemArray[0].ToString();
            tbAppunto.Text = Note.Rows[ElencoNote.CurrentCell.RowIndex].ItemArray[1].ToString();
            Modifica = true;
        }

        private void ElencoNote_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void siticoneCirclePictureBox2_Click(object sender, EventArgs e)
        {
            tbTitolo.Text = "";
            tbAppunto.Text = "";
        }

        private void siticoneCirclePictureBox4_Click(object sender, EventArgs e)
        {
            try
            {
                Note.Rows[ElencoNote.CurrentCell.RowIndex].Delete();
            }
            catch (Exception ex) { Console.WriteLine("Nota non valida"); }
        }

        private void siticoneCirclePictureBox3_Click(object sender, EventArgs e)
        {
            if (Modifica)
            {
                Note.Rows[ElencoNote.CurrentCell.RowIndex]["Titolo"] = tbTitolo.Text;
                Note.Rows[ElencoNote.CurrentCell.RowIndex]["Appunto"] = tbAppunto.Text;
                Note.Rows[ElencoNote.CurrentCell.RowIndex]["Argomento"] = tbArgomento.Text;


            }
            else
            {
                Note.Rows.Add(tbTitolo.Text, tbAppunto.Text);
            }
            tbTitolo.Text = "";
            tbAppunto.Text = "";
            Modifica = false;
        }

        private void siticoneCirclePictureBox1_Click(object sender, EventArgs e)
        {
            tbTitolo.Text = Note.Rows[ElencoNote.CurrentCell.RowIndex].ItemArray[0].ToString();
            tbAppunto.Text = Note.Rows[ElencoNote.CurrentCell.RowIndex].ItemArray[1].ToString();
            Modifica = true;
        }

        private void tbAppunto_TextChanged(object sender, EventArgs e)
        {

        }

        private void siticoneCirclePictureBox1_Click_1(object sender, EventArgs e)
        {
            FrmMateria frmMateria = new FrmMateria();
            frmMateria.FormClosed += frmMateria_FormClosed;
            DialogResult dr = frmMateria.ShowDialog(this);
        }

        private void frmMateria_FormClosed(object sender, FormClosedEventArgs e)
        {
            popolaCB();
        }

        private void PbCancellaTesto_Click(object sender, EventArgs e)
        {
            tbAppunto.Text = "";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public int ElencoNoteHeight
        {
            get { return ElencoNote.Width; }
            set { ElencoNote.Height = value; }
        }
        public int TbAppuntoHeight
        {
            get { return tbAppunto.Height; }
            set { tbAppunto.Height = value; }
        }

        public int TbAppuntoWidth
        {
            get { return tbAppunto.Width; }
            set { tbAppunto.Width = value; }
        }

        public int PbSalvaAppuntoSlideX
        {
            get { return PbSalvaAppunto.Location.X; }
            set
            {
                PbSalvaAppunto.Location = new Point(value, PbSalvaAppunto.Location.Y);
            }
        }

        public int PbCancellaTestoSlideX
        {
            get {return PbCancellaTesto.Location.X;}
            set
            {
                PbCancellaTesto.Location = new Point(value, PbCancellaTesto.Location.Y);
            }

        }
    }
}
