﻿namespace Appunti_Badi_Licci_Pardo
{
    partial class FrmCaricamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerCaricamento = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ProgressoCircolare = new Siticone.Desktop.UI.WinForms.SiticoneCircleProgressBar();
            this.timerCaricamentoCircolare = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.ProgressoCircolare.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerCaricamento
            // 
            this.timerCaricamento.Enabled = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Appunti_Badi_Licci_Pardo.Properties.Resources.loginMatch;
            this.pictureBox1.Location = new System.Drawing.Point(29, 41);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(103, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // ProgressoCircolare
            // 
            this.ProgressoCircolare.Controls.Add(this.pictureBox1);
            this.ProgressoCircolare.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(213)))), ((int)(((byte)(218)))), ((int)(((byte)(223)))));
            this.ProgressoCircolare.FillThickness = 5;
            this.ProgressoCircolare.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.ProgressoCircolare.ForeColor = System.Drawing.Color.White;
            this.ProgressoCircolare.Location = new System.Drawing.Point(145, 33);
            this.ProgressoCircolare.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProgressoCircolare.Minimum = 0;
            this.ProgressoCircolare.Name = "ProgressoCircolare";
            this.ProgressoCircolare.ProgressColor = System.Drawing.SystemColors.Highlight;
            this.ProgressoCircolare.ProgressColor2 = System.Drawing.Color.White;
            this.ProgressoCircolare.ProgressThickness = 5;
            this.ProgressoCircolare.ShadowDecoration.Mode = Siticone.Desktop.UI.WinForms.Enums.ShadowMode.Circle;
            this.ProgressoCircolare.Size = new System.Drawing.Size(160, 160);
            this.ProgressoCircolare.TabIndex = 3;
            this.ProgressoCircolare.Text = "siticoneCircleProgressBar1";
            // 
            // timerCaricamentoCircolare
            // 
            this.timerCaricamentoCircolare.Enabled = true;
            this.timerCaricamentoCircolare.Tick += new System.EventHandler(this.timerCaricamentoCircolare_Tick);
            // 
            // FrmCaricamento
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(231)))), ((int)(((byte)(250)))));
            this.ClientSize = new System.Drawing.Size(453, 320);
            this.Controls.Add(this.ProgressoCircolare);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FrmCaricamento";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmCaricamento";
            this.Load += new System.EventHandler(this.FrmCaricamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ProgressoCircolare.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerCaricamento;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Siticone.Desktop.UI.WinForms.SiticoneCircleProgressBar ProgressoCircolare;
        private System.Windows.Forms.Timer timerCaricamentoCircolare;
    }
}