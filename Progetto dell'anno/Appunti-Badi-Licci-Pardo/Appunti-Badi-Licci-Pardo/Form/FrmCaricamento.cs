﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appunti_Badi_Licci_Pardo
{
    public partial class FrmCaricamento : Form
    {
        public FrmCaricamento()
        {
            InitializeComponent();

        }

        
        

        private void timerCaricamentoCircolare_Tick(object sender, EventArgs e)
        {
            ProgressoCircolare.Value = ProgressoCircolare.Value + 3;

            if (ProgressoCircolare.Value >= 99)
            {
                ProgressoCircolare.Value = 0;
                FrmLogin FrmLogin = new FrmLogin();
                DialogResult dr = FrmLogin.ShowDialog(this);
            }

        }

        private void FrmCaricamento_Load(object sender, EventArgs e)
        {

        }
    }
}
