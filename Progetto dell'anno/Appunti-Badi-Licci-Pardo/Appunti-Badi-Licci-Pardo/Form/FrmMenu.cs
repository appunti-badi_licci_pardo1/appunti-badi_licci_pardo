﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appunti_Badi_Licci_Pardo
{
    public partial class FrmMenu : Form
    {
        bool fullscreen = false;

        FrmAppunto frmAppunto = new FrmAppunto();


        public FrmMenu()
        {
            InitializeComponent();
            Personalizzadesign();
        }
        private void Personalizzadesign()
        {
            panelAppunti.Visible = false;
            panelAISubMenu.Visible = false;
        }

        private void NascondiSubMenu()
        {
            //pannello appunti
            if (panelAppunti.Visible == true)
                panelAppunti.Visible = false;
            //pannello AI
            if (panelAISubMenu.Visible == true)
                panelAISubMenu.Visible = false;
        }

        private void MostraSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                NascondiSubMenu();
                subMenu.Visible = true;

            }
            else
                subMenu.Visible = false;
        }

   
        private void btAiuto_Click(object sender, EventArgs e)
        {
            NascondiSubMenu();
        }
        
        private Form FormAttiva = null;
        private void apriFormSecondaria (Form formSecondaria)
        {
            if (FormAttiva != null)
                FormAttiva.Close();
            FormAttiva = formSecondaria;
            formSecondaria.TopLevel = false;
            formSecondaria.FormBorderStyle = FormBorderStyle.None;
            formSecondaria.Dock = DockStyle.Fill;
            
        }
        

      
        private void btVisualizzaAppunti_Click_1(object sender, EventArgs e)
        {
            // form lista appunti
            NascondiSubMenu();
        }

        private void btAppunti_Click_1(object sender, EventArgs e)
        {
            MostraSubMenu(panelAppunti);
        }

        private void btCreaAppunti_Click(object sender, EventArgs e)
        {
            btCreaAppunti.Enabled = false;

            if (frmAppunto == null || frmAppunto.IsDisposed)
                frmAppunto = new FrmAppunto(); 

            FrmAttiva = frmAppunto;
            frmAppunto.TopLevel = false;
            frmAppunto.FormBorderStyle = FormBorderStyle.None;
            frmAppunto.Dock = DockStyle.Fill;

            if (!panelFormFiglia.Controls.Contains(frmAppunto))
                panelFormFiglia.Controls.Add(frmAppunto);

            panelFormFiglia.Tag = frmAppunto;
            frmAppunto.BringToFront();
            frmAppunto.Show();

            NascondiSubMenu();

            btCreaAppunti.Enabled = true;
        }


        private void btFlashBud_Click(object sender, EventArgs e)
        {
            // form ai
            ApriFormFlashBud(new FrmFlashBud());
            NascondiSubMenu();
        }

        private void FlashBud_Click(object sender, EventArgs e)
        {
            MostraSubMenu(panelAISubMenu);
        }

        private Form FrmAttiva = null;

        private void ApriFormFlashBud(Form frmFiglia)
        {
            btCreaAppunti.Enabled = true;
            panelFormFiglia.Controls.Clear();

            if (FrmAttiva != null)
                FrmAttiva.Close();
            FrmAttiva = frmFiglia;
            frmFiglia.TopLevel = false;
            frmFiglia.FormBorderStyle = FormBorderStyle.None;
            frmFiglia.Dock = DockStyle.Fill;
            panelFormFiglia.Controls.Add(frmFiglia);
            panelFormFiglia.Tag = frmFiglia;
            frmFiglia.BringToFront();
            frmFiglia.Show();

        }


        private void panelSideMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        #region RIDIMENSIONAMENTO FULLSCREEN FORM APPUNTO
        private void FrmMenu_Resize(object sender, EventArgs e)
        {
            if (fullscreen)
            {
                frmAppunto.ElencoNoteHeight = 602;

                frmAppunto.TbAppuntoHeight = 428;
                frmAppunto.TbAppuntoWidth = 674;

                frmAppunto.PbSalvaAppuntoSlideX = 1076;
                frmAppunto.PbCancellaTestoSlideX = 1133;

            }
            else
            {
                frmAppunto.ElencoNoteHeight = 980;
                frmAppunto.TbAppuntoHeight = 806;
                frmAppunto.PbSalvaAppuntoSlideX = 1600;
                frmAppunto.PbCancellaTestoSlideX = 1657;

                frmAppunto.TbAppuntoWidth = 1200;

            }
            fullscreen = !fullscreen;
        }
        #endregion


    }
}
