﻿namespace Appunti_Badi_Licci_Pardo
{
    partial class FrmMenù
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.panelAISubMenu = new System.Windows.Forms.Panel();
            this.btFlashBud = new System.Windows.Forms.Button();
            this.FlashBud = new System.Windows.Forms.Button();
            this.panelTraduttoreSubMenu = new System.Windows.Forms.Panel();
            this.btTraduci = new System.Windows.Forms.Button();
            this.btTraduttore = new System.Windows.Forms.Button();
            this.panelAppunti = new System.Windows.Forms.Panel();
            this.btCreaAppunti = new System.Windows.Forms.Button();
            this.btVisualizzaAppunti = new System.Windows.Forms.Button();
            this.btAppunti = new System.Windows.Forms.Button();
            this.btAiuto = new System.Windows.Forms.Button();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelPerLogo = new System.Windows.Forms.Panel();
            this.labelTitolo = new System.Windows.Forms.Label();
            this.panelSideMenu.SuspendLayout();
            this.panelAISubMenu.SuspendLayout();
            this.panelTraduttoreSubMenu.SuspendLayout();
            this.panelAppunti.SuspendLayout();
            this.panelLogo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.SystemColors.Highlight;
            this.panelSideMenu.Controls.Add(this.panelAISubMenu);
            this.panelSideMenu.Controls.Add(this.FlashBud);
            this.panelSideMenu.Controls.Add(this.panelTraduttoreSubMenu);
            this.panelSideMenu.Controls.Add(this.btTraduttore);
            this.panelSideMenu.Controls.Add(this.panelAppunti);
            this.panelSideMenu.Controls.Add(this.btAppunti);
            this.panelSideMenu.Controls.Add(this.btAiuto);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(250, 553);
            this.panelSideMenu.TabIndex = 0;
            // 
            // panelAISubMenu
            // 
            this.panelAISubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelAISubMenu.Controls.Add(this.btFlashBud);
            this.panelAISubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAISubMenu.Location = new System.Drawing.Point(0, 371);
            this.panelAISubMenu.Name = "panelAISubMenu";
            this.panelAISubMenu.Size = new System.Drawing.Size(250, 46);
            this.panelAISubMenu.TabIndex = 12;
            // 
            // btFlashBud
            // 
            this.btFlashBud.BackColor = System.Drawing.Color.White;
            this.btFlashBud.Dock = System.Windows.Forms.DockStyle.Top;
            this.btFlashBud.FlatAppearance.BorderSize = 0;
            this.btFlashBud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btFlashBud.Location = new System.Drawing.Point(0, 0);
            this.btFlashBud.Name = "btFlashBud";
            this.btFlashBud.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btFlashBud.Size = new System.Drawing.Size(250, 45);
            this.btFlashBud.TabIndex = 0;
            this.btFlashBud.Text = "Chatta con FlashBud";
            this.btFlashBud.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btFlashBud.UseVisualStyleBackColor = false;
            this.btFlashBud.Click += new System.EventHandler(this.btFlashBud_Click);
            // 
            // FlashBud
            // 
            this.FlashBud.BackColor = System.Drawing.SystemColors.Highlight;
            this.FlashBud.Dock = System.Windows.Forms.DockStyle.Top;
            this.FlashBud.FlatAppearance.BorderSize = 0;
            this.FlashBud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FlashBud.ForeColor = System.Drawing.Color.White;
            this.FlashBud.Location = new System.Drawing.Point(0, 326);
            this.FlashBud.Name = "FlashBud";
            this.FlashBud.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.FlashBud.Size = new System.Drawing.Size(250, 45);
            this.FlashBud.TabIndex = 13;
            this.FlashBud.Text = "FlashBud";
            this.FlashBud.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FlashBud.UseVisualStyleBackColor = false;
            this.FlashBud.Click += new System.EventHandler(this.FlashBud_Click);
            // 
            // panelTraduttoreSubMenu
            // 
            this.panelTraduttoreSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelTraduttoreSubMenu.Controls.Add(this.btTraduci);
            this.panelTraduttoreSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTraduttoreSubMenu.Location = new System.Drawing.Point(0, 282);
            this.panelTraduttoreSubMenu.Name = "panelTraduttoreSubMenu";
            this.panelTraduttoreSubMenu.Size = new System.Drawing.Size(250, 44);
            this.panelTraduttoreSubMenu.TabIndex = 10;
            // 
            // btTraduci
            // 
            this.btTraduci.BackColor = System.Drawing.Color.White;
            this.btTraduci.Dock = System.Windows.Forms.DockStyle.Top;
            this.btTraduci.FlatAppearance.BorderSize = 0;
            this.btTraduci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTraduci.Location = new System.Drawing.Point(0, 0);
            this.btTraduci.Name = "btTraduci";
            this.btTraduci.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btTraduci.Size = new System.Drawing.Size(250, 45);
            this.btTraduci.TabIndex = 0;
            this.btTraduci.Text = "Traduci un testo scritto";
            this.btTraduci.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btTraduci.UseVisualStyleBackColor = false;
            this.btTraduci.Click += new System.EventHandler(this.btTraduci_Click);
            // 
            // btTraduttore
            // 
            this.btTraduttore.BackColor = System.Drawing.SystemColors.Highlight;
            this.btTraduttore.Dock = System.Windows.Forms.DockStyle.Top;
            this.btTraduttore.FlatAppearance.BorderSize = 0;
            this.btTraduttore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btTraduttore.ForeColor = System.Drawing.Color.White;
            this.btTraduttore.Location = new System.Drawing.Point(0, 237);
            this.btTraduttore.Name = "btTraduttore";
            this.btTraduttore.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btTraduttore.Size = new System.Drawing.Size(250, 45);
            this.btTraduttore.TabIndex = 11;
            this.btTraduttore.Text = "Traduttore";
            this.btTraduttore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btTraduttore.UseVisualStyleBackColor = false;
            this.btTraduttore.Click += new System.EventHandler(this.btTraduttore_Click_2);
            // 
            // panelAppunti
            // 
            this.panelAppunti.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panelAppunti.Controls.Add(this.btCreaAppunti);
            this.panelAppunti.Controls.Add(this.btVisualizzaAppunti);
            this.panelAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAppunti.Location = new System.Drawing.Point(0, 145);
            this.panelAppunti.Name = "panelAppunti";
            this.panelAppunti.Size = new System.Drawing.Size(250, 92);
            this.panelAppunti.TabIndex = 1;
            // 
            // btCreaAppunti
            // 
            this.btCreaAppunti.BackColor = System.Drawing.Color.White;
            this.btCreaAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.btCreaAppunti.FlatAppearance.BorderSize = 0;
            this.btCreaAppunti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btCreaAppunti.Location = new System.Drawing.Point(0, 45);
            this.btCreaAppunti.Name = "btCreaAppunti";
            this.btCreaAppunti.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btCreaAppunti.Size = new System.Drawing.Size(250, 45);
            this.btCreaAppunti.TabIndex = 1;
            this.btCreaAppunti.Text = "Crea Appunti";
            this.btCreaAppunti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCreaAppunti.UseVisualStyleBackColor = false;
            this.btCreaAppunti.Click += new System.EventHandler(this.btCreaAppunti_Click);
            // 
            // btVisualizzaAppunti
            // 
            this.btVisualizzaAppunti.BackColor = System.Drawing.Color.White;
            this.btVisualizzaAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.btVisualizzaAppunti.FlatAppearance.BorderSize = 0;
            this.btVisualizzaAppunti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btVisualizzaAppunti.Location = new System.Drawing.Point(0, 0);
            this.btVisualizzaAppunti.Name = "btVisualizzaAppunti";
            this.btVisualizzaAppunti.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btVisualizzaAppunti.Size = new System.Drawing.Size(250, 45);
            this.btVisualizzaAppunti.TabIndex = 0;
            this.btVisualizzaAppunti.Text = "I miei appunti";
            this.btVisualizzaAppunti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btVisualizzaAppunti.UseVisualStyleBackColor = false;
            this.btVisualizzaAppunti.Click += new System.EventHandler(this.btVisualizzaAppunti_Click_1);
            // 
            // btAppunti
            // 
            this.btAppunti.BackColor = System.Drawing.SystemColors.Highlight;
            this.btAppunti.Dock = System.Windows.Forms.DockStyle.Top;
            this.btAppunti.FlatAppearance.BorderSize = 0;
            this.btAppunti.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAppunti.ForeColor = System.Drawing.Color.White;
            this.btAppunti.Location = new System.Drawing.Point(0, 100);
            this.btAppunti.Name = "btAppunti";
            this.btAppunti.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btAppunti.Size = new System.Drawing.Size(250, 45);
            this.btAppunti.TabIndex = 9;
            this.btAppunti.Text = "Appunti";
            this.btAppunti.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAppunti.UseVisualStyleBackColor = false;
            this.btAppunti.Click += new System.EventHandler(this.btAppunti_Click_1);
            // 
            // btAiuto
            // 
            this.btAiuto.FlatAppearance.BorderSize = 0;
            this.btAiuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btAiuto.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btAiuto.Location = new System.Drawing.Point(0, 508);
            this.btAiuto.Name = "btAiuto";
            this.btAiuto.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btAiuto.Size = new System.Drawing.Size(250, 45);
            this.btAiuto.TabIndex = 8;
            this.btAiuto.Text = "Aiuto";
            this.btAiuto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAiuto.UseVisualStyleBackColor = true;
            // 
            // panelLogo
            // 
            this.panelLogo.BackColor = System.Drawing.SystemColors.Highlight;
            this.panelLogo.Controls.Add(this.labelTitolo);
            this.panelLogo.Controls.Add(this.panelPerLogo);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(250, 100);
            this.panelLogo.TabIndex = 0;
            // 
            // panelPerLogo
            // 
            this.panelPerLogo.BackgroundImage = global::Appunti_Badi_Licci_Pardo.Properties.Resources.LOGO3;
            this.panelPerLogo.Location = new System.Drawing.Point(3, 26);
            this.panelPerLogo.Name = "panelPerLogo";
            this.panelPerLogo.Size = new System.Drawing.Size(52, 48);
            this.panelPerLogo.TabIndex = 1;
            // 
            // labelTitolo
            // 
            this.labelTitolo.AutoSize = true;
            this.labelTitolo.Font = new System.Drawing.Font("Broadway", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitolo.ForeColor = System.Drawing.Color.White;
            this.labelTitolo.Location = new System.Drawing.Point(61, 42);
            this.labelTitolo.Name = "labelTitolo";
            this.labelTitolo.Size = new System.Drawing.Size(176, 32);
            this.labelTitolo.TabIndex = 2;
            this.labelTitolo.Text = "FLASHPAD";
            // 
            // FrmMenù
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 553);
            this.Controls.Add(this.panelSideMenu);
            this.MinimumSize = new System.Drawing.Size(950, 600);
            this.Name = "FrmMenù";
            this.Text = "FrmMenù";
            this.panelSideMenu.ResumeLayout(false);
            this.panelAISubMenu.ResumeLayout(false);
            this.panelTraduttoreSubMenu.ResumeLayout(false);
            this.panelAppunti.ResumeLayout(false);
            this.panelLogo.ResumeLayout(false);
            this.panelLogo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Button btAiuto;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Button btAppunti;
        private System.Windows.Forms.Panel panelAppunti;
        private System.Windows.Forms.Button btCreaAppunti;
        private System.Windows.Forms.Button btVisualizzaAppunti;
        private System.Windows.Forms.Panel panelAISubMenu;
        private System.Windows.Forms.Button btFlashBud;
        private System.Windows.Forms.Button FlashBud;
        private System.Windows.Forms.Panel panelTraduttoreSubMenu;
        private System.Windows.Forms.Button btTraduci;
        private System.Windows.Forms.Button btTraduttore;
        private System.Windows.Forms.Panel panelPerLogo;
        private System.Windows.Forms.Label labelTitolo;
    }
}