﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appunti_Badi_Licci_Pardo
{
    public partial class FrmMenù : Form
    {
        public FrmMenù()
        {
            InitializeComponent();
            Personalizzadesign();
        }
        private void Personalizzadesign()
        {
            panelAppunti.Visible = false;
            panelTraduttoreSubMenu.Visible = false;
            panelAISubMenu.Visible = false;
        }

        private void NascondiSubMenu()
        {
            //pannello appunti
            if (panelAppunti.Visible == true)
                panelAppunti.Visible = false;
            //pannello traduttore
            if (panelTraduttoreSubMenu.Visible == true)
                panelTraduttoreSubMenu.Visible = false;
            //pannello AI
            if (panelAISubMenu.Visible == true)
                panelAISubMenu.Visible = false;
        }

        private void MostraSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                NascondiSubMenu();
                subMenu.Visible = true;

            }
            else
                subMenu.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }


       

      
     
     

      

        private void btAiuto_Click(object sender, EventArgs e)
        {
            NascondiSubMenu();
        }
        private Form FormAttiva = null;
        private void apriFormSecondaria (Form formSecondaria)
        {
            if (FormAttiva != null)
                FormAttiva.Close();
            FormAttiva = formSecondaria;
            formSecondaria.TopLevel = false;
            formSecondaria.FormBorderStyle = FormBorderStyle.None;
            formSecondaria.Dock = DockStyle.Fill;
            
        }

        private void panelTraduttoreSubMenu_Paint(object sender, PaintEventArgs e)
        {

        }
        

        private void btChatAI_Click_1(object sender, EventArgs e)
        {

        }

        private void panelAppunti_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btVisualizzaAppunti_Click_1(object sender, EventArgs e)
        {
            // form lista appunti
            NascondiSubMenu();
        }

        private void btAppunti_Click_1(object sender, EventArgs e)
        {
            MostraSubMenu(panelAppunti);
        }

        private void btCreaAppunti_Click(object sender, EventArgs e)
        {
            //form creazione appunto
            NascondiSubMenu();
        }

        private void btTraduttore_Click_2(object sender, EventArgs e)
        {
            MostraSubMenu(panelTraduttoreSubMenu);
        }

        private void btTraduci_Click(object sender, EventArgs e)
        {
            //form traduttore
        }

        private void btFlashBud_Click(object sender, EventArgs e)
        {
            // form ai
            NascondiSubMenu();
        }

        private void FlashBud_Click(object sender, EventArgs e)
        {
            MostraSubMenu(panelAISubMenu);
        }
    }
}
