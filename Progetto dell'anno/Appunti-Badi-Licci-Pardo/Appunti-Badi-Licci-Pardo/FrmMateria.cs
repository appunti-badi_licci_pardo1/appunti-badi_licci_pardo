﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Appunti_Badi_Licci_Pardo
{
    public partial class FrmMateria : Form
    {
        public FrmMateria()
        {
            InitializeComponent();
        }

        private void btnSalva_Click(object sender, EventArgs e)
        {
            string codiceMateria = tbCodiceMateria.Text;
            string nomeMateria = tbNomeMateria.Text;

            if (string.IsNullOrEmpty(codiceMateria) || string.IsNullOrEmpty(nomeMateria))
            {
                MessageBox.Show("Hai dimenticato di inserire i dati");
            }

            string materia = $"{codiceMateria} - {nomeMateria}";
            Program._listaMaterie.Add(materia);

            MessageBox.Show("Dati salvati con successo!");

            tbCodiceMateria.Text = "";
            tbNomeMateria.Text = "";
        }
    }
}
